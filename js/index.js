$(document).ready(function(){

		/* To Display Date Picker*/ 
	    $( "#dobpicker" ).datepicker({
	      changeMonth: true,//this option for allowing user to selementct month
	      changeYear: true //this option for allowing user to selementct from year range
	    });

	    /* To Get Phone Number Input In Us Format*/ 
	    var phones = [{ "mask": "(###) ###-####"}];
	    $('input[name=phone_number]').inputmask({ 
		        mask: phones, 
		        greedy: false, 
		        definitions: { '#': { validator: "[0-9]", cardinality: 1}}
         });


	    /* Form Validation Starts*/ 
	    $('#myform').on('submit',function(event){
	    	
	    	event.preventDefault();
			
	    	/* Validation For First Name Starts*/ 
	    	if($('input[name=fname]').val()=="")
	    	{
	    		var element='input[name=fname]';
	    		var text='Please Fill First Name';
	    		error(element,text);

	    	}
	    	else{
	    		var element='input[name=fname]';
	    		var fname=$(element).val();
	    		if(fname.match("^[a-zA-Z()]+$")==null)
	    		{
	    			text=' First Name In Alphabets';
	    			error(element,text);
	    		}
	    		else
	    		{
	    			defaultcss(element);
	    		}
	    		
	    	}
	    	/* Validation For First Name Ends*/ 

	    	/* Validation For Last Name Starts*/ 
	    	if($('input[name=lname]').val()=="")
	    	{
	    		var element='input[name=lname]';
	    		var text='Please Fill Last Name';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=lname]';
	    		var lname=$(element).val();
	    		if(lname.match("^[a-zA-Z()]+$")==null)
	    		{
	    			text=' Last Name In Alphabets';
	    			error(element,text);
	    		}
	    		else
	    		{
	    			defaultcss(element);
	    		}
	    	}	
	    	/* Validation For Last Name Ends*/ 

	    	/* Validation For Phone Number Starts*/ 
	    	if($('input[name=phone_number]').val()=="")
	    	{
	    		var element='input[name=phone_number]';
	    		var text='Please Fill Phone Number';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=phone_number]';
	    		var phone_number=$(element).val();
	    		var count=0;
	    		for(var i=0;i<phone_number.length;i++)
	    		{
	   				if(phone_number.charAt(i)=='_')
	   					count=count+1;
	    		}
	    		if(count>0)
	    		{
	    			text='Please Enter 10 Digit Number'
	    			error(element,text);
	    		}
		    	else
	    			defaultcss(element);
	    	}
	    	/* Validation For Phone Number Ends*/

	    	/* Validation For Date Of Birth Starts*/ 
	    	if($('input[name=dob]').val()=="")
	    	{
	    		var element='input[name=dob]';
	    		var text='Please Fill Date Of Birth';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=dob]';
	    		var dob=$(element).val();
	    		var month=dob.charAt(0)+dob.charAt(1);
	    		var day=dob.charAt(3)+dob.charAt(4);
	    		var year=dob.charAt(6)+dob.charAt(7)+dob.charAt(8)+dob.charAt(9);

	    		var age = 18;
				var mydate = new Date();
				var currdate = new Date();
				currdate.setFullYear(currdate.getFullYear() - age);
				if ((currdate - mydate) < 0){
					text='Age Limit 18 years';
					error(element,text);
				}
				else
	    		defaultcss(element);
	    	}
	    	/* Validation For Date Of Birth Ends*/

	    	/* Validation For Location Starts*/
	    	if($('textarea[name=location]').val()=="")
	    	{
	    		var element='textarea[name=location]';
	    		var text='Please Fill location';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=location]';
	    		defaultcss(element);
	    	}
		  	/* Validation For Location Ends*/

		  	/* Validation For Email Starts*/
	    	if($('input[name=email]').val()=="")
	    	{
	    		var element='input[name=email]';
	    		var text='Please Fill Email';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=email]';
	    		var email=$('input[name=email]').val();
	    		var atindex=email.indexOf('@');
	    		var dotindex=email.indexOf('.');
	    		if (atindex< 1 || dotindex<atindex+2 || dotindex+2>=email.length) {
	    			var text='Please Fill Valid Email';
			       error(element,text);
			    }
			    else
			    {
			    	defaultcss(element);
			    }
	    	}
	    	/* Validation For Email Ends*/

	    	/* Validation For Password Starts*/
	    	if($('input[name=password]').val()=="")
	    	{
	    		var element='input[name=password]';
	    		var text='Please Fill Password';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=password]';
	    		defaultcss(element);
	    	}
	    	/* Validation For Password Ends*/

	    	/* Validation For Confirm Password Starts*/
	    	if($('input[name=confirm_password]').val()=="")
	    	{
	    		var element='input[name=confirm_password]';
	    		var text='Please Fill Confirm Password';
	    		error(element,text);
	    	}
	    	else{
	    		var element='input[name=confirm_password]';
	    		var password=$('input[name=password]').val();
	    		var confirm_password=$(element).val();
	    		if(password!="" && password!=confirm_password)
	    		{
	    			var text='Please Fill This Same As Password';
	    			error(element,text);
	    		}
	    		else
	    		defaultcss(element);
	    	}
	    	/* Validation For Confirm Password Ends*/

	    	/* Validation For Checkbox Starts*/
	    	if($('.checkbox_btn:checkbox:checked').length == 0)
	    	{
	    		var element=$('.hobby');
	    		var text='Select At least One Hobby';
	    		error(element,text);
	    	}
	    	else
	    	{
	    		var element=$('.hobby');
	    		defaultcss(element);

	    	}
	    	/* Validation For Checkbox Ends*/	

	    	
	    });

		/* Common Function Error() Starts */
		function error(element,text){
			$(element).next('.error_icon').css('display','block');
	    		$(element).next('.error_icon').next('.error_msg').html(text);
	    		$(element).next('.error_icon').mouseenter(function(e){
	    			$(element).next('.error_icon').next('.error_msg').css('display','block');
	    			$(element).next('.error_icon').next('.error_msg').next('.triangle').css('display','block');
	    		});
	    		$(element).next('.error_icon').mouseleave(function(e){
	    			$(element).next('.error_icon').next('.error_msg').css('display','none');
	    			$(element).next('.error_icon').next('.error_msg').next('.triangle').css('display','none');
	    		});
	    		return;
		};
		/* Common Function Error() Ends */

		/* Common Function Defaultcss() Starts */
		function defaultcss(element){
			$(element).next('.error_icon').css('display','none');
			return;
		};
		/* Common Function Defaultcss() Ends */
		
});
		/* Form Validation Ends*/ 