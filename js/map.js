// To Show User Entered Location On Google Map And Get Its Lat And Long
$('#map').locationpicker({
	location: {latitude: 46.15242437752303, longitude: 2.7470703125}, //DEFAULT LATITUDE AND LONGITUDE VALUE	
	radius: 300,
	inputBinding: {
		latitudeInput: $('#latitude'),
		longitudeInput: $('#longitude'),
		locationNameInput: $('#location')        
	},
	enableAutocomplete: true,
	onchanged: function(currentLocation, radius, isMarkerDropped) {
			$("#latlong").fadeIn(1000);
		}
});

// To Display Empty Location, Latitude And Longitude Initially
$(window).load(function(){
	$('#location').val('');
	$('#latitude').val('');
	$('#longitude').val('');
});